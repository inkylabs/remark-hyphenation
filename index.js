import { promises as fs } from 'fs'

// const NAME = 'hyphenation'

const WORD_RE = /\w+/g

let _wordMap = null
async function initWords (file) {
  if (!_wordMap) {
    _wordMap = {}
    if (file) {
      const data = await fs.readFile(file, 'utf8')
      for (const line of data.split('\n')) {
        if (!line) continue
        _wordMap[line.replace(/-/g, '')] = line.replace(/-/g, '\u00AD')
      }
    }
  }
  return _wordMap
}

export default (opts) => {
  opts = Object.assign({
    ignoretypes: []
  }, opts)
  return async (root, f) => {
    await initWords(opts.file)

    let enabled = true
    const handleNode = async n => {
      if (opts.ignoretypes.includes(n.type)) return
      switch (n.type) {
        case 'text-nohyphens':
          enabled = false
          n.type = 'span'
          break
        case 'text-yeshyphens':
          enabled = true
          n.type = 'span'
          break
        case 'text':
          if (!enabled) break
          n.value = n.value.replace(WORD_RE, m => {
            const r = _wordMap[m]
            if (r) return r
            return m
          })
          break
        default:
          for (const c of n.children || []) {
            await handleNode(c)
          }
      }
    }
    await handleNode(root)
  }
}
